﻿using MessageManager.Core.Authentication.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageManager.Domain.Security.Accounts.Models
{

    public class ClientContext : IClientContext
    {
        public string Key { get; set; }
        public int UserId { get; set; }
    }
}
