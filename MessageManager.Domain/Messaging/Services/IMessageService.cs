﻿using System;
using System.Collections.Generic;
using System.Text;
using MessageManager.Data.Models.Messaging;

namespace MessageManager.Domain.Messaging.Services
{
    /// <summary>
    /// <see cref="MessageService"/>
    /// </summary>
	public interface IMessageService
	{
		void SendMessage(string content, string from, string to);
	}
	
}
