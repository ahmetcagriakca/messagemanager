﻿using System;
using System.Collections.Generic;
using System.Text;
using MessageManager.Data.Models.Accounts;

namespace MessageManager.Data.Models.Messaging
{
	public class Message : Entity<int>
    {
		public string Content { get; set; }
		public DateTime SendDateTime { get; set; }
		public User From { get; set; }
		public User To { get; set; }
	}
}
