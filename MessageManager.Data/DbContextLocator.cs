﻿using MessageManager.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageManager.Data
{
	public class DbContextLocator : IDbContextLocator
	{
		private readonly MessageManagerDbContext _dbContext;
		public DbContextLocator(MessageManagerDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		DbContext IDbContextLocator.Current
		{
			get
			{
				return _dbContext;
			}
		}
	}
}
