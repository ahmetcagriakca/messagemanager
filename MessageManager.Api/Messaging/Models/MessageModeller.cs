﻿using MessageManager.Data.Models.Accounts;

namespace MessageManager.Api.Security.Accounts.Models
{
    public class SendMessageRequest
	{
        public string Content { get; set; }
        public string To { get; set; }
    }
}
