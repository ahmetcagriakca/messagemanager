﻿using MessageManager.Api.Security.Accounts.Models;
using MessageManager.Core.Authentication.Services;
using MessageManager.Core.Controllers;
using MessageManager.Domain.Messaging.Services;
using MessageManager.Domain.Security.Accounts.Models;
using Microsoft.AspNetCore.Mvc;

namespace MessageManager.Api.Security.Accounts.Controllers
{
	public class MessageController : BaseController
	{
		private readonly IMessageService messageService;
		private readonly IAuthenticationService authenticationService;

		public MessageController(IMessageService messageService,
			IAuthenticationService authenticationService)
		{
			this.messageService = messageService;
			this.authenticationService = authenticationService;
		}

		[HttpPost]
		public IActionResult SendMessage([FromBody]SendMessageRequest request)
		{
			var from = authenticationService.GetContext<ClientContext>().Key;
			messageService.SendMessage(request.Content, from, request.To);
			return Ok(new { IsSuccess = true });
		}
	}
}