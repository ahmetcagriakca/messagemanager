﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageManager.Core.Authentication.Exceptions
{
    public class AuthenticationTokenNotFoundException : Exception
    {
        public AuthenticationTokenNotFoundException(string message) : base(message)
        {
        }
    }
}
