﻿namespace MessageManager.Core.Authentication.Models
{
    public interface IClientContext
    {
        string Key { get; set; }
    }
}
