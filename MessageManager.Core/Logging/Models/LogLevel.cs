﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageManager.Core.Logging.Models
{
    public enum LogLevel
    {
        Fatal,
        Error,
        Warn,
        Info,
        Debug,
        Trace
    }
}
