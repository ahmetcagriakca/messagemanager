﻿using MessageManager.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageManager.Test.Helpers
{
    public class TestDbHelper: ITestDbHelper
	{
		private IDbContextLocator Locator { get; }

		IDbContextLocator ITestDbHelper.Locator => Locator;

		public TestDbHelper()
		{
			var options = new DbContextOptionsBuilder<MessageManagerDbContext>()
					 .UseInMemoryDatabase(Guid.NewGuid().ToString())
					 .Options;
			MessageManagerDbContext context = new MessageManagerDbContext(options);
			Locator = new DbContextLocator(context);
		}
		
		public void SaveChanges()
		{
			Locator.Current.SaveChanges();
		}
	}
}
