﻿using MessageManager.Data;

namespace MessageManager.Test.Helpers
{
	public interface ITestDbHelper
	{
		IDbContextLocator Locator { get; }
		void SaveChanges();
	}
}